package hu.fornax.tua.entity;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import hu.fornax.tua.config.dto.enums.ConfigDataType;
import hu.fornax.tua.config.dto.enums.ConfigType;

/**
 * The persistent class for the config database table.
 */
@Entity
@Cacheable
@Table(name = "config")
@NamedQueries({
        @NamedQuery(name = "Config.findAll", query = "SELECT c FROM Config c"),
        @NamedQuery(name = "Config.findByName", query = "SELECT c FROM Config c WHERE c.name = :name"),
        @NamedQuery(name = "Config.findByNames", query = "SELECT c FROM Config c WHERE c.name IN :names"),
        @NamedQuery(name = "Config.findByConfigType", query = "SELECT c FROM Config c WHERE c.configType = :configType ORDER BY c.id"),
})
// @Audited
public class Config implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Access(AccessType.PROPERTY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "config_type")
    private ConfigType configType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "data_type")
    private ConfigDataType dataType;

    @NotNull
    @Column(name = "required")
    private Boolean required;

    @Size(max = 1000)
    @Column(name = "val")
    private String value;

    @Size(max = 1000)
    @Column(name = "label")
    private String label;

    @Column(name = "help")
    private String help;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConfigType getConfigType() {
        return configType;
    }

    public void setConfigType(ConfigType configType) {
        this.configType = configType;
    }

    public ConfigDataType getDataType() {
        return dataType;
    }

    public void setDataType(ConfigDataType dataType) {
        this.dataType = dataType;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

}
