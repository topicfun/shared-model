package hu.fornax.tua.config.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.camel.Header;

import hu.fornax.exception.GeneralException;
import hu.fornax.tua.common.log.Logger;
import hu.fornax.tua.config.dto.ConfigDto;
import hu.fornax.tua.config.dto.message.ConfigMessage;
import hu.fornax.tua.config.protocol.service.ConfigService;
import hu.fornax.tua.esb.protocol.message.JBPMCommandType;
import hu.fornax.tua.esb.protocol.message.JBPMMessage;
import hu.fornax.tua.esb.protocol.message.JBPMMessageBody;
import hu.fornax.tua.esb.protocol.message.JBPMMessageHeader;
import hu.fornax.tua.esb.protocol.util.ObjectMapping;

/**
 * <p>
 * <b>ConfigController osztály.</b>
 * </p>
 *
 * @author Zoltan Balogh <br>
 * 
 *         <p>
 *         <b>Attribútumok:</b>
 *         </p>
 *         <ul>
 *         <li></li>
 *         </ul>
 */
@Stateless
@Named("configController")
public class ConfigController {

    /** A/Az log. */
    @Inject
    private Logger log;

    /** A/Az config service. */
    @Inject
    ConfigService configService;

    /**
     * Process config request.
     * </b>
     * </p>
     * <b>Backlog címe</b>: <a href="link">...</a><br>
     * <b>Backlog leírása</b>:<br>
     * <br>
     *
     * @param jbpmMessage
     *            A(z) jbpm message
     * @param method
     *            A(z) method
     * @return JBPMMessage a JBPMMessage
     *         <br>
     *         <br>
     *         <b>Test: </b>
     * @throws Exception
     *             the exception
     * @see ...Test hivatkozás...
     */
    public JBPMMessage processConfigRequest(JBPMMessage jbpmMessage, @Header("type") String method)
            throws Exception {

        JBPMCommandType commandType = JBPMCommandType.valueOf(method);

        log.debug("FileController commad type:{}", commandType);

        ConfigMessage configMessage;

        switch (commandType) {
        case CONFIG_READ_REQUEST:
            configMessage = ObjectMapping.JsonToObject(jbpmMessage.getJBPMMessageBody().getjBPMServiceMessage(),
                    ConfigMessage.class);

            String configName = configMessage.getConfigName();

            if (configName != null && !configName.isEmpty()) {
                log.debug("Config name: {}", configName);

                ConfigDto configDto = configService.getConfigByName(configName);
                configMessage.setConfigValue(configDto.getValue());
                log.debug("Get config value: {}", configMessage.getConfigValue());
            }

            List<String> configNames = configMessage.getConfigNames();
            if (configNames != null && !configNames.isEmpty()) {
                log.debug("Config names: {}", configNames);

                Map<String, String> configValues = configService.getConfigByNames(configNames);

                Set<String> notFoundConfigs = new HashSet<>(configNames);
                notFoundConfigs.removeAll(configValues.keySet());
                if (!notFoundConfigs.isEmpty()) {
                    throw new IllegalStateException(
                            "Ezek a config-ok nem találhatóak az adatbázisban: " + String.join(", ", notFoundConfigs));
                }

                configMessage.setConfigValues(configValues);
            }

            return createJBPMMessage(JBPMCommandType.CONFIG_READ_RESPONSE, configMessage);

        case CONFIG_WRITE_REQUEST:

            log.entryPoint(jbpmMessage);
            configMessage = ObjectMapping.JsonToObject(jbpmMessage.getJBPMMessageBody().getjBPMServiceMessage(),
                    ConfigMessage.class);
            log.debug("Config name: {}", configMessage.getConfigName());
            log.debug("Set config value: {}", configMessage.getConfigValue());
            Objects.requireNonNull(configMessage.getConfigName());

            configService.setConfigByName(configMessage.getConfigName(), configMessage.getConfigValue());

            return createJBPMMessage(JBPMCommandType.CONFIG_WRITE_RESPONSE, configMessage);

        default:
            log.error("ConfigService method not implemented!");
            break;
        }

        throw new GeneralException("ConfigService method not implemented!");

    }

    /**
     * Létrehozza a JBPM message.
     * </b>
     * </p>
     * <b>Backlog címe</b>: <a href="link">...</a><br>
     * <b>Backlog leírása</b>:<br>
     * <br>
     *
     * @param jbpmCommandType
     *            A(z) jbpm command type
     * @param configMessage
     *            A(z) config message
     * @return JBPMMessage a JBPMMessage
     *         <br>
     *         <br>
     *         <b>Test: </b>
     * @throws Exception
     *             the exception
     * @see ...Test hivatkozás...
     */
    private JBPMMessage createJBPMMessage(JBPMCommandType jbpmCommandType, ConfigMessage configMessage)
            throws Exception {

        JBPMMessageHeader header = new JBPMMessageHeader();
        JBPMMessageBody jbpmMessageBody = new JBPMMessageBody();
        JBPMMessage jbpmMessage = new JBPMMessage();
        jbpmMessage.setJBPMMessageBody(jbpmMessageBody);
        jbpmMessage.setJBPMMessageHeader(header);

        header.setType(jbpmCommandType);
        jbpmMessageBody.setjBPMServiceMessage(ObjectMapping.ObjectToJson(configMessage));

        return jbpmMessage;

    }
}
