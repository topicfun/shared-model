package hu.fornax.tua.config.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import hu.fornax.tua.config.dto.enums.ConfigType;
import hu.fornax.tua.config.protocol.dao.ConfigDAOIface;
import hu.fornax.tua.entity.Config;
import hu.fornax.tua.model.facade.CRUD;

public class ConfigDAO extends CRUD<Config> implements ConfigDAOIface {
    public ConfigDAO() {
        super(Config.class);
    }

    @Override
    public Config getConfig(String configName) {
        TypedQuery<Config> query = em.createNamedQuery("Config.findByName", Config.class);
        query.setParameter("name", configName);
        return query.getSingleResult();
    }

    @Override
    public List<Config> getConfigList(List<String> configNameList) {
        TypedQuery<Config> query = em.createNamedQuery("Config.findByNames", Config.class);
        query.setParameter("names", configNameList);
        return query.getResultList();
    }

    @Override
    public List<Config> getConfigList(ConfigType configType) {

        List<Config> configs = getAllEntity();
        System.out.println("configs" + configs);
        return configs;

        // TypedQuery<Config> query = em.createNamedQuery("Config.findByConfigType", Config.class);
        // query.setParameter("configType", configType);
        // return query.getResultList();
    }
}
