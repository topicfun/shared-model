package hu.fornax.tua.config.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import hu.fornax.exception.GeneralExceptionBuilder;
import hu.fornax.tua.common.log.Logger;
import hu.fornax.tua.config.dto.ConfigDto;
import hu.fornax.tua.config.dto.enums.ConfigDataType;
import hu.fornax.tua.config.dto.enums.ConfigType;
import hu.fornax.tua.config.protocol.dao.ConfigDAOIface;
import hu.fornax.tua.config.protocol.service.ConfigService;
import hu.fornax.tua.entity.Config;
import hu.fornax.tua.util.rest.exception.ExceptionUiCode;
import hu.fornax.tua.util.rest.exception.ParameterKeyUiCode;
import hu.fornax.tua.util.rest.exception.ValidationException;

@Stateless
@Named("configServiceImpl")
public class ConfigServiceImpl implements ConfigService {
    @Inject
    private ConfigDAOIface configDAO;
    @Inject
    private Logger log;
    // @Inject
    // private FileFacade fileFacade;

    public static ConfigDto getConfigDto(Config config) {
        ConfigDto dto = new ConfigDto();
        dto.setId(config.getId());
        dto.setValue(config.getValue());
        dto.setLabel(config.getLabel());
        dto.setDataType(config.getDataType());
        dto.setRequired(config.getRequired());
        dto.setHelp(config.getHelp());
        dto.setName(config.getName());
        return dto;
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.controller.ConfigService#listConfig()
     */
    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#listConfig()
     */
    @Override
    public List<ConfigDto> listConfig(ConfigType configType) {
        return configDAO.getConfigList(configType).stream().map(ConfigServiceImpl::getConfigDto)
                .collect(Collectors.toList());
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.controller.ConfigService#getConfigById(java.lang.Long)
     */
    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#getConfigById(java.lang.Long)
     */
    @Override
    public ConfigDto getConfigById(Long id) {
        Config config = configDAO.getEntity(id);
        if (config == null) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameterName.name(), "id")
                    .addParameter(ParameterKeyUiCode.parameterValue.name(), id.toString())
                    .addExceptionUiCode(ExceptionUiCode.DATABASE_RECORD_NOT_EXISTS)
                    .asException();
        }
        return getConfigDto(config);
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.controller.ConfigService#getConfigByName(java.lang.String)
     */
    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#getConfigByName(java.lang.String)
     */
    @Override
    public ConfigDto getConfigByName(String configName) {
        try {
            Config config = configDAO.getConfig(configName);
            return getConfigDto(config);
        } catch (NoResultException e) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameterName.name(), "name")
                    .addParameter(ParameterKeyUiCode.parameterValue.name(), configName)
                    .addExceptionUiCode(ExceptionUiCode.DATABASE_RECORD_NOT_EXISTS)
                    .asException();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#getConfigByNames(java.util.List)
     */
    @Override
    public Map<String, String> getConfigByNames(List<String> configNameList) {

        try {

            return configDAO.getConfigList(configNameList)
                    .stream().collect(Collectors
                            .toMap(Config::getName, Config::getValue));

        } catch (NoResultException e) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameterName.name(), "names")
                    .addParameter(ParameterKeyUiCode.parameterValue.name(), configNameList.toString())
                    .addExceptionUiCode(ExceptionUiCode.DATABASE_RECORD_NOT_EXISTS)
                    .asException();
        }

    }

    private String validateErtek(String ertek, ConfigDataType tipus) {
        if (ertek != null && !ertek.isEmpty()) {
            if (ertek.length() > 1000) {
                ertek = ertek.substring(0, 1000);
            }
            boolean invalidInput = false;
            switch (tipus) {
            case INT:
                try {
                    Long.parseLong(ertek);
                } catch (NumberFormatException e) {
                    invalidInput = true;
                }
                break;
            case DATE:
                try {
                    long szam = Long.parseLong(ertek);
                    if (szam < 1) {
                        invalidInput = true;
                    }
                } catch (NumberFormatException e) {
                    invalidInput = true;
                }
                break;
            case EMAIL_TEMPLATE:
                try {
                    long szam = Long.parseLong(ertek);
                    if (szam < 1) {
                        invalidInput = true;
                    }
                } catch (NumberFormatException e) {
                    invalidInput = true;
                }
                break;
            // case FILE:
            // try {
            // long fileId = Long.parseLong(ertek);
            // if (fileId < 1) {
            // invalidInput = true;
            // } else {
            // if (fileFacade.getEntity(fileId) == null) {
            // invalidInput = true;
            // }
            // }
            // } catch (NumberFormatException e) {
            // invalidInput = true;
            // }
            // break;
            default:
                break;
            }
            if (invalidInput) {
                throw GeneralExceptionBuilder.create(ValidationException.class)
                        .addParameter(ParameterKeyUiCode.parameterName.name(), "value")
                        .addParameter(ParameterKeyUiCode.parameterValue.name(), ertek)
                        .addExceptionUiCode(ExceptionUiCode.INVALID_INPUT_FORMAT)
                        .asException();
            }
        }
        return ertek;
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.controller.ConfigService#setConfigById(java.lang.Long, hu.fornax.tua.config.dto.ConfigDto)
     */
    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#setConfigById(java.lang.Long, hu.fornax.tua.config.dto.ConfigDto)
     */
    @Override
    public ConfigDto setConfigById(Long id, ConfigDto configDto, ConfigType configType) {
        if (!id.equals(configDto.getId())) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.id1.name(), id.toString())
                    .addParameter(ParameterKeyUiCode.id2.name(), configDto.getId().toString())
                    .addExceptionUiCode(ExceptionUiCode.ID_MISMATCH)
                    .asException();
        }

        Config config = configDAO.getEntity(id);
        if (config == null || config.getConfigType() != configType) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameterName.name(), "id")
                    .addParameter(ParameterKeyUiCode.parameterValue.name(), id.toString())
                    .addExceptionUiCode(ExceptionUiCode.DATABASE_RECORD_NOT_EXISTS)
                    .asException();
        }

        String ertek = validateErtek(configDto.getValue(), config.getDataType());
        config.setValue(ertek);

        return getConfigDto(config);
    }

    /*
     * (non-Javadoc)
     *
     * @see hu.fornax.tua.config.service.ConfigService#setConfigByName(java.lang.String, java.lang.String)
     */
    @Override
    public void setConfigByName(String name, String value) {
        Config config = configDAO.getConfig(name);
        if (config == null) {
            throw GeneralExceptionBuilder.create(ValidationException.class)
                    .addParameter(ParameterKeyUiCode.parameterName.name(), "name")
                    .addParameter(ParameterKeyUiCode.parameterValue.name(), name)
                    .addExceptionUiCode(ExceptionUiCode.DATABASE_RECORD_NOT_EXISTS)
                    .asException();
        }

        String ertek = validateErtek(value, config.getDataType());
        config.setValue(ertek);
    }

}
