package hu.fornax.tua.config.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import hu.fornax.tua.util.rest.RestURI;

@ApplicationPath(RestURI.BASE)
public class ApplicationConfig extends Application {

}
