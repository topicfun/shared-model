package hu.fornax.tua.config.route;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;

import hu.fornax.tua.util.esb.temp.ESBLog;

@ApplicationScoped
@ContextName("camel-config-context")
public class ConfigRoute extends RouteBuilder {
    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.builder.RouteBuilder#configure()
     */
    @Override
    public void configure() throws Exception {

        onException(Exception.class)
                .handled(true)
                .log(ESBLog.ESB_ERROR + "Config error sent back to the client")
                .bean("exceptionHandler")
                // .marshal().json(JsonLibrary.Jackson)
                .log(ESBLog.ESB_ERROR + "ERROR in Config prosessing request! ${body} ")
                .end();

        from("activemq:queue:CONFIG.INOUT.EP")
                .log(ESBLog.CONFIG + "incoming ESB request body: ${body} ${headers}")
                .bean("configController")
                .log(ESBLog.CONFIG + "returning ESB response body: ${body} ${headers}");

    }
}
