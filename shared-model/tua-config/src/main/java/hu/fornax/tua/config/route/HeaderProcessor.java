package hu.fornax.tua.config.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class HeaderProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        Object[] args = exchange.getIn().getBody(Object[].class);

        System.out.println("args: " + args);

    }

}
