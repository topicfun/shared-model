package hu.fornax.tua.config.rest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.ejb3.annotation.SecurityDomain;

import hu.fornax.tua.common.log.Logger;
import hu.fornax.tua.config.dto.ConfigDto;
import hu.fornax.tua.config.dto.enums.ConfigType;
import hu.fornax.tua.config.protocol.rest.ConfigRest;
import hu.fornax.tua.config.protocol.service.ConfigService;

@Named("configRestImpl")
@SecurityDomain("keycloak")
public class ConfigRestImpl implements ConfigRest {
    @Inject
    private ConfigService configService;
    @Inject
    private Logger log;

    @Override
    public Response getConfigById(Long id) {
        log.entryPoint(id);
        ConfigDto result = configService.getConfigById(id);
        log.debug("GET /config/{} result: {}", id, result);
        return Response.status(Status.OK).entity(result).build();
    }

    @Override
    public Response getConfigByName(String name) {
        log.entryPoint(name);
        ConfigDto result = configService.getConfigByName(name);
        log.debug("GET /config/{} result: {}", name, result);
        return Response.status(Status.OK).entity(result).build();
    }

    private Response listConfig(ConfigType configType) {
        log.entryPoint(configType);
        List<ConfigDto> result = configService.listConfig(configType);
        log.debug("GET /config/{} result size: {}", configType.name().toLowerCase(), result.size());
        return Response.status(Status.OK).entity(result).build();
    }

    @Override
    public Response listAlkalmazasConfig() {
        return listConfig(ConfigType.ALKALMAZAS);
    }

    @Override
    public Response listRendszerConfig() {
        return listConfig(ConfigType.RENDSZER);
    }

    private Response setConfig(Long id, ConfigDto configDto, ConfigType configType) {
        log.entryPoint(id, configDto, configType);
        ConfigDto result = configService.setConfigById(id, configDto, configType);
        log.debug("PUT /config/{}/{} result: {}", configType.name().toLowerCase(), id, result);
        return Response.status(Status.OK).entity(result).build();
    }

    @Override
    public Response setAlkalmazasConfig(Long id, ConfigDto configDto) {
        return setConfig(id, configDto, ConfigType.ALKALMAZAS);
    }

    @Override
    public Response setRendszerConfig(Long id, ConfigDto configDto) {
        return setConfig(id, configDto, ConfigType.RENDSZER);
    }

}
