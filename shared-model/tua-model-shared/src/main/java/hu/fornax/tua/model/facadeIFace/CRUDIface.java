package hu.fornax.tua.model.facadeIFace;

import java.util.List;

import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

public interface CRUDIface<T> {

    void addEntity(T entity);

    T getEntity(Long id);

    T getEntity(Long id, Boolean archiv);

    T getEntityCache(Long id);

    void deleteEntity(Long id);

    T updateEntity(T entity);

    List<T> getAllEntity();

    void lock(T entity, LockModeType lockModeType);

    Predicate stringSearch(String parameter, CriteriaBuilder cb,
            Expression<String> rootExpression);

}
