package hu.fornax.tua.model.facade;

import java.util.List;

import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import hu.fornax.tua.model.facadeIFace.CRUDIface;

public class CRUD<T> extends AbstractDAO<T> implements CRUDIface<T> {
    public CRUD(Class<T> entityClass) {
        super(entityClass);
    }

    public CRUD() {
    }

    @Override
    public void addEntity(T entity) {
        persist(entity);
    }

    @Override
    public T getEntity(Long id) {
        return find(id);
    }

    @Override
    public T getEntity(Long id, Boolean archiv) {
        return find(id, archiv);
    }

    @Override
    public T getEntityCache(Long id) {
        return findCache(id);
    }

    @Override
    public void deleteEntity(Long id) {
        remove(id);
    }

    @Override
    public T updateEntity(T entity) {
        return merge(entity);
    }

    @Override
    public List<T> getAllEntity() {
        return findAll();
    }

    @Override
    public void lock(T entity, LockModeType lockModeType) {
        em.lock(entity, lockModeType);
    }

    @Override
    public Predicate stringSearch(String parameter, CriteriaBuilder cb,
            Expression<String> rootExpression) {
        String REGEXP = "[ ]+";
        String param = parameter.toLowerCase().replaceAll(REGEXP, "");
        Expression<String> lowerCase = cb.lower(rootExpression);

        return cb.like(cb.function("REGEXP_REPLACE", String.class, lowerCase, cb.literal(REGEXP), cb.literal(""),
                cb.literal('g')),
                "%" + param + "%");
    }
}