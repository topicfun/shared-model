package hu.fornax.tua.model.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.EntityType;

//import hu.fornax.tua.common.log.Logger;

public class AbstractDAO<T> {

    protected Class<T> entityClass;

    @PersistenceContext(unitName = "org.tua.domain")
    public EntityManager em;

    @PersistenceContext(unitName = "org.tua.domain.archiv")
    public EntityManager emArchiv;

    /**
     * inject log.
     */
    // @Inject
    // private Logger log;

    public AbstractDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public AbstractDAO() {
        super();
    }

    protected EntityManager getEntityManager() {
        return this.em;
    }

    protected void setEntityManager(EntityManager em) {
        this.em = em;
    }

    protected EntityManager getEntityManagerArchiv() {
        return this.emArchiv;
    }

    protected void setEntityManagerArchiv(EntityManager emArchiv) {
        this.emArchiv = emArchiv;
    }

    /**
     * Retrieves the meta-model for a certain entity.
     *
     * @return the meta-model of a certain entity.
     */
    protected EntityType<T> getMetaModel() {
        return getEntityManager().getMetamodel().entity(entityClass);
    }

    public void persist(T entity) {
        getEntityManager().persist(entity);
    }

    public T merge(T entity) {
        return getEntityManager().merge(entity);
    }

    public void remove(Object entityId) {
        T entity = find(entityId);

        if (entity != null)
            getEntityManager().remove(entity);
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public T find(Object id, Boolean archiv) {

        EntityManager entityManager;

        if (archiv != null && archiv) {
            entityManager = getEntityManagerArchiv();
        } else
            entityManager = getEntityManager();

        return entityManager.find(entityClass, id);
    }

    public T findCache(Object id) {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("org.hibernate.cacheable", "true");
        getEntityManager().setProperty("org.hibernate.cacheable", "true");
        getEntityManager().setProperty("org.hibernate.cacheMode", org.hibernate.CacheMode.REFRESH);

        System.out.println("getEntityManager().getProperties(): " + getEntityManager().getProperties());
        Cache cache = getEntityManager().getEntityManagerFactory().getCache();
        System.out.println("cache: " + cache);

        return getEntityManager().find(entityClass, id, properties);
    }

    public List<T> findAll() {
        CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public int count() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<T> root = cq.from(entityClass);
        cq.select(cb.count(root));
        return getEntityManager().createQuery(cq).getSingleResult().intValue();
    }

    /**
     * Visszaadja a lista első elemét, vagy null-t ha üres a lista.
     *
     * @param list
     * @return
     * @author Matyi
     */
    public static <T> T getFirst(List<T> list) {

        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    /**
     * A criteria lekérdezés általános objektumait tárolja. {@link getQuery()
     * getQuery} hozza létre. CriteriaBuilder, CriteriaQuery, Root.
     *
     * @param <E>
     * @author Matyi
     */
    public static class CQuery<E> {
        public CriteriaBuilder builder;
        public CriteriaQuery<E> query;
        public Root<E> root;

        private boolean If = true;

        private ArrayList<Predicate> ps;

        public CQuery<E> If(boolean b) {
            If = b;
            return this;
        }

        public CQuery<E> IfNotNull(Object o) {
            If = o != null;
            return this;
        }

        private void add(Predicate p) {
            if (ps == null) {
                ps = new ArrayList<>();
            }
            if (If) {
                ps.add(p);
            } else {
                If = true;
            }
        }

        public CQuery<E> and(Predicate p) {
            add(p);
            return this;
        }

        public CQuery<E> equal(String attrName, Object value) {
            if (value != null) {
                add(builder.equal(root.get(attrName), value));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> equal(String attrName, String attrName2, Object value) {
            if (value != null) {
                add(builder.equal(root.get(attrName).get(attrName2), value));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> equal(String attrName, String attrName2, String attrName3, Object value) {
            if (value != null) {
                add(builder.equal(root.get(attrName).get(attrName2).get(attrName3), value));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> equal(Join<?, ?> table, String attrName, Object value) {
            if (value != null) {
                add(builder.equal(table.get(attrName), value));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> equalNull(String attrName, Object value) {
            add(builder.equal(root.get(attrName), value));
            return this;
        }

        public CQuery<E> like(String attrName, Object value) {
            if (value != null) {
                add(builder.like(builder.lower(root.get(attrName)), "%" + value.toString().toLowerCase() + "%"));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> like(String attrName, String attrName2, Object value) {
            if (value != null) {
                add(builder.like(builder.lower(root.get(attrName).get(attrName2)), "%" + value.toString().toLowerCase() + "%"));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> isFalse(String attrName) {
            add(builder.isFalse(root.get(attrName)));
            return this;
        }

        public CQuery<E> betweenTwoDates(String attrName, Date valueFrom, Date valueTo) {
            if (valueFrom != null) {
                add(builder.greaterThanOrEqualTo(root.get(attrName), valueFrom));
            }
            if (valueTo != null) {
                add(builder.lessThanOrEqualTo(root.get(attrName), valueTo));
            }
            return this;
        }

        public CQuery<E> exists(Subquery<?> subquery) {
            if (subquery != null) {
                add(builder.exists(subquery));
            } else {
                If = true;
            }
            return this;
        }

        /**
         * Predicate: attrName1 <= value <= attrName2
         * or attrName1 <= value & attrName2 is null
         * 
         * @param attrName1
         * @param attrName2
         * @param value
         * @return
         */
        public <Y extends Comparable<? super Y>> CQuery<E> between2ndCanBeNull(
                String attrName1, String attrName2, Y value) {
            if (If && value != null) {
                Predicate twoDate = builder.between(builder.literal(value),
                        root.get(attrName1), root.get(attrName2));
                Predicate oneDate = builder.and(
                        builder.lessThanOrEqualTo(root.get(attrName1), value),
                        root.get(attrName2).isNull());
                add(builder.or(twoDate, oneDate));
            } else {
                If = true;
            }
            return this;
        }

        public CQuery<E> orderBy(String attrName) {
            query.orderBy(builder.asc(root.get(attrName)));
            return this;
        }

        public CQuery<E> orderByDesc(String attrName) {
            query.orderBy(builder.desc(root.get(attrName)));
            return this;
        }

        private void endWhere() {
            if (ps != null && !ps.isEmpty()) {
                query.where(ps.toArray(new Predicate[ps.size()]));
            }
        }

        public TypedQuery<E> createQuery(EntityManager em) {
            endWhere();
            return em.createQuery(query);
        }

        public List<E> getResultList(EntityManager em) {
            return createQuery(em).getResultList();
        }

        public E getFirstResult(EntityManager em) {
            List<E> resultList = createQuery(em).setMaxResults(1).getResultList();
            if (resultList.isEmpty())
                return null;
            return resultList.get(0);
        }
    }

    protected List<T> getResultList(CQuery<T> q) {
        return em.createQuery(q.query).getResultList();
    }

    /**
     * Létrehozza a CQuery objektumot a lekérdezéshez.
     *
     * @return
     * @author Matyi
     */
    protected CQuery<T> getQuery() {
        CQuery<T> q = new CQuery<>();
        q.builder = getEntityManager().getCriteriaBuilder();
        q.query = q.builder.createQuery(entityClass);
        q.root = q.query.from(entityClass);
        return q;
    }

    /**
     * Megadhatjuk egy mező értékét a lekérdezéshez. Lehet nem egyenlőséget is
     * megadni.
     *
     * @author Matyi
     */
    public static class Filter {
        String attributeName;
        String attributeName2;
        Object value;
        Boolean negate = false;

        /**
         * Ha egy entitás mező értékét akarjuk megadni. Pl: new
         * Filter("kiadoFelhasznalo", "azon", 13L).
         *
         * @param attributeName
         * @param attributeName2
         * @param value
         */
        public Filter(String attributeName, String attributeName2, Object value) {
            this.attributeName = attributeName;
            this.attributeName2 = attributeName2;
            this.value = value;
        }

        /**
         * Ha egy mező értékét akarjuk megadni. Pl: new Filter("sorszam",
         * 1000L).
         *
         * @param attributeName
         * @param value
         */
        public Filter(String attributeName, Object value) {
            this.attributeName = attributeName;
            this.value = value;
        }

        /**
         * Ha nem egyenlőséget (!=) akarunk megadni, negate = true.
         *
         * @param attributeName
         * @param value
         * @param negate
         */
        public Filter(String attributeName, Object value, Boolean negate) {
            this.attributeName = attributeName;
            this.value = value;
            this.negate = negate;
        }

    }

    /**
     * Lista lekérdezése {@link Filter Filter}-ek alapján.
     *
     * @param filters
     * @return
     * @author Matyi
     */
    public List<T> getListByFilter(Filter... filters) {
        CQuery<T> q = getQuery();
        Predicate[] ps = new Predicate[filters.length];
        int i = 0;
        for (Filter filter : filters) {
            if (filter.attributeName2 == null)
                if (filter.negate) {
                    ps[i] = q.builder.notEqual(q.root.get(filter.attributeName), filter.value);
                } else {
                    ps[i] = q.builder.equal(q.root.get(filter.attributeName), filter.value);
                }
            else
                ps[i] = q.builder.equal(q.root.get(filter.attributeName).get(filter.attributeName2), filter.value);
            i++;
        }
        q.query.where(q.builder.and(ps));
        List<T> result = getEntityManager().createQuery(q.query).getResultList();
        return result;
    }

    /**
     * Lista lekérdezése {@link Filter Filter}-ek alapján, 'OR' kapcsolat
     *
     * @param filters
     * @return
     * @author Zoltán Balogh
     */
    public List<T> getListByFilterOr(Filter... filters) {
        CQuery<T> q = getQuery();
        Predicate[] ps = new Predicate[filters.length];
        int i = 0;
        for (Filter filter : filters) {
            if (filter.attributeName2 == null)
                if (filter.negate) {
                    ps[i] = q.builder.notEqual(q.root.get(filter.attributeName), filter.value);
                } else {
                    ps[i] = q.builder.equal(q.root.get(filter.attributeName), filter.value);
                }
            else
                ps[i] = q.builder.equal(q.root.get(filter.attributeName).get(filter.attributeName2), filter.value);
            i++;
        }
        q.query.where(q.builder.or(ps));
        List<T> result = getEntityManager().createQuery(q.query).getResultList();

        return result;

    }

    /**
     * Lista Cache lekérdezése {@link Filter Filter}-ek alapján.
     *
     * @param filters
     * @return
     * @author Zoltán Balogh
     */
    public List<T> getListByFilterCache(Filter... filters) {

        CQuery<T> q = getQuery();
        Predicate[] ps = new Predicate[filters.length];
        int i = 0;
        for (Filter filter : filters) {
            if (filter.attributeName2 == null)
                if (filter.negate) {
                    ps[i] = q.builder.notEqual(q.root.get(filter.attributeName), filter.value);
                } else {
                    ps[i] = q.builder.equal(q.root.get(filter.attributeName), filter.value);
                }
            else
                ps[i] = q.builder.equal(q.root.get(filter.attributeName).get(filter.attributeName2), filter.value);
            i++;
        }
        q.query.where(q.builder.and(ps));
        List<T> result = getEntityManager()
                .createQuery(q.query)
                .setHint("org.hibernate.cacheable", "true")
                .getResultList();

        return result;
    }

    /**
     * Egy eredmény lekérdezése {@link Filter Filter}-ek alapján.
     *
     * @param filters
     * @return
     * @author Matyi
     */
    public T getFirstByFilter(Filter... filters) {
        return getFirst(getListByFilter(filters));
    }

    /**
     * Visszaadja a result objektumokat.
     *
     * @param query
     *            A(z) query
     * @return result objektum.
     */
    public T getResult(TypedQuery<T> query) {
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }
}